﻿using WindowsInput;

namespace AutoScrollLock
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var @is = new InputSimulator();

            @is.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.SCROLL);
        }
    }
}
